#!/bin/sh

# read environment variables from env.sh
. ./env.sh

# compile project with included script
mvn clean package || exit 1

case $1 in
    java)
        # run the app
        java -jar target/constellation-0.0.1-SNAPSHOT.jar
        ;;
    docker)
        # (re)build docker image
        docker build -t team2/constellation . || exit 1

        # run the docker image
        docker run \
            -e SERVER_PORT="$SERVER_PORT" \
            -e DB_URL="$DB_URL" \
            -e DB_USER="$DB_USER" \
            -e DB_PASS="$DB_PASS" \
            -e API_KEY="$API_KEY" \
            -e SEARCH_ENGINE_ID="$SEARCH_ENGINE_ID" \
            -e SALT="$SALT" \
            --rm \
            -p 8080:8080 \
            --network="host" \
            --name constellation \
            team2/constellation
        ;;
    *)
        # run the app
        java -jar target/constellation-0.0.1-SNAPSHOT.jar
        ;;
esac