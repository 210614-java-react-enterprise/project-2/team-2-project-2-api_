insert into users values (default, 'elvis@lee.com', true, 'elvis', 'elvisl');
insert into users values (default, 'wyatt@goettsch.com', true, 'wyatt', 'wyattg');
insert into users values (default, 'rensy@aikara.com', true, 'rensy', 'aikara');

insert into bookmark values (default, 3, true, 'search', 'Google', 'https://www.google.com');

insert into user_bookmark values (1,1);
insert into user_bookmark values (2,1);
insert into user_bookmark values (3,1);