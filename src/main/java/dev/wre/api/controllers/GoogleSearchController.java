package dev.wre.api.controllers;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin
@RequestMapping("/googlesearch")
public class GoogleSearchController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${API_KEY}")
    private String apiKey;   ////enter apiKey here
    @Value("${SEARCH_ENGINE_ID}")
    private String searchEngineID;    /////enter search engine id here


    @GetMapping(value = "/{searchTopic}", produces = "application/json")
    public ResponseEntity<String> getGoogleSearch(@PathVariable("searchTopic") String searchTopic) throws JSONException {
        String url = "https://www.googleapis.com/customsearch/v1?key="+apiKey+"&cx="+searchEngineID+"&q="+searchTopic;
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        JSONObject jsonString = new JSONObject(responseEntity.getBody());
        String items = jsonString.getString("items");
        return new ResponseEntity<>(items, HttpStatus.OK);
    }
}
