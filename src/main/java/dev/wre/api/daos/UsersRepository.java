package dev.wre.api.daos;

import dev.wre.api.models.Bookmark;
import dev.wre.api.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsersRepository extends JpaRepository<Users,Integer> {

    Users findByUsername(String username);

}
